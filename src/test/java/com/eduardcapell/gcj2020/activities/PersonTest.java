package com.eduardcapell.gcj2020.activities;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonTest {

    Person person = null;

    final static int NORMAL_TASK_START = 100;
    final static int NORMAL_TASK_END = 200;
    final static int BEFORE_NORMAL_TASK = 50;
    final static int AFTER_NORMAL_TASK = 300;

    final static int BEFORE_INSTANT_TASK = 500;
    final static int INSTANT_TASK_START = 1000;
    final static int INSTANT_TASK_END = 1000;
    final static int AFTER_INSTANT_TASK = 2000;

    @Before
    public void setup() {
        person = new Person('A');
    }

    @Test
    public void shouldBeFreeBeforeAnyTaskAssigned() {
        assertTrue(person.free(0));
        assertTrue(person.free(1000));
        assertTrue(person.free(100000));
    }

    @Test
    public void shouldBeFreeBeforeNormalTaskStarts() {
        configureNormalTask();
        assertTrue(person.free(BEFORE_NORMAL_TASK));
    }

    @Test
    public void shouldBeFreeAfterNormalTaskEnds() {
        configureNormalTask();
        assertTrue(person.free(AFTER_NORMAL_TASK));
    }

    @Test
    public void shouldBeFreeWithInstantTask() {
        configureInstantTask();
        assertTrue(person.free(INSTANT_TASK_START));
    }

    @Test
    public void shouldBeFreeBeforeInstantTaskBegins() {
        configureInstantTask();
        assertTrue(person.free(BEFORE_INSTANT_TASK));
    }

    @Test
    public void shouldBeFreeAfterInstantTaskEnds() {
        configureInstantTask();
        assertTrue(person.free(AFTER_INSTANT_TASK));
    }

    @Test
    public void shouldBeFreeWhenNormalTaskEnds() {
        configureNormalTask();

        assertTrue(person.free(NORMAL_TASK_END));
    }

    @Test
    public void shouldNotBeFreeWhenNormalTaskStarts() {
        configureNormalTask();
        assertFalse(person.free(NORMAL_TASK_START));
    }


    private void configureNormalTask() {
        person.setStart(NORMAL_TASK_START);
        person.setEnd(NORMAL_TASK_END);
    }

    private void configureInstantTask() {
        person.setStart(INSTANT_TASK_START);
        person.setEnd(INSTANT_TASK_END);
    }

}
